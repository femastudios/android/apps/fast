package com.femastudios.fast

import org.junit.Assert.*
import org.junit.Test

class UtilsTest {

    @Test
    fun tokenize() {
        assertEquals(listOf("TV", "Series"), Utils.tokenize("TVSeries"))
        assertEquals(listOf("TV", "Series"), Utils.tokenize("TV Series"))
        assertEquals(listOf("Movies", "Fad"), Utils.tokenize("MoviesFad"))
        assertEquals(listOf("Whats", "App"), Utils.tokenize("WhatsApp"))
        assertEquals(listOf("Super", "SU"), Utils.tokenize("SuperSU"))
        assertEquals(listOf("Wi", "Fi", "ADB"), Utils.tokenize("WiFi ADB"))
        assertEquals(listOf("What", "if?"), Utils.tokenize("What if?"))
        assertEquals(listOf("SQ", "Lite"), Utils.tokenize("SQLite"))
        assertEquals(listOf("Juice", "SSH"), Utils.tokenize("JuiceSSH"))
        assertEquals(listOf("Last", "Pass"), Utils.tokenize("LastPass"))

        assertTrue(Utils.tokensMatchQuery(listOf("TV", "Series"), "TVSeries"))
        assertTrue(Utils.tokensMatchQuery(listOf("TV", "Series"), "TV Series"))
        assertTrue(Utils.tokensMatchQuery(listOf("TV", "Series"), "TV Seri"))
        assertTrue(Utils.tokensMatchQuery(listOf("TV", "Series"), "TVSe"))
        assertTrue(Utils.tokensMatchQuery(listOf("TV", "Series"), "tvse"))
        assertTrue(Utils.tokensMatchQuery(listOf("TV", "Series"), "series"))
        assertFalse(Utils.tokensMatchQuery(listOf("TV", " Series"), "seriess"))
        assertFalse(Utils.tokensMatchQuery(listOf("TV", " Series"), "vser"))
        assertFalse(Utils.tokensMatchQuery(listOf("TV", " Series"), "ttv"))
        assertTrue(Utils.tokensMatchQuery(listOf("TV", " Series"), ""))
        assertTrue(Utils.tokensMatchQuery(listOf("Google", "Play", "Store"), "play store"))

        //Initials
        assertTrue(Utils.tokensMatchQuery(listOf("TV", "Series"), "ts"))
        assertTrue(Utils.tokensMatchQuery(listOf("Last", "Pass"), "lp"))

    }

}