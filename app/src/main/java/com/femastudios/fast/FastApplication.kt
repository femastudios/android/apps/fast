/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.femastudios.dataflow.DataflowThreadUtils
import com.femastudios.dataflow.imageloader.ImageLoader
import com.femastudios.fast.data.AppIconLoader
import com.femastudios.fast.data.ContactPhotoLoader

class FastApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        PackageBroadcastReceiver.register()
        DataflowThreadUtils.execute {
            ImageLoader.addResourceLoader(this, AppIconLoader)
            ImageLoader.addResourceLoader(this, ContactPhotoLoader)
        }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    companion object {
        lateinit var instance: FastApplication
            private set
    }
}