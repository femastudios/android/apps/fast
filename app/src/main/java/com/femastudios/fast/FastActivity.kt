/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Space
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.femastudios.android.core.dp
import com.femastudios.android.core.setPadding
import com.femastudios.dataflow.android.declarativeui.add
import com.femastudios.dataflow.android.declarativeui.setContent
import com.femastudios.dataflow.android.listen
import com.femastudios.dataflow.android.viewState.extensions.setTwoWayText
import com.femastudios.dataflow.async.Loaded
import com.femastudios.dataflow.async.extensions.valueOr
import com.femastudios.dataflow.async.util.async
import com.femastudios.dataflow.extensions.mapIndexed
import com.femastudios.dataflow.util.mutableFieldOf
import com.femastudios.fast.Utils.openHowToUseDialog
import com.femastudios.fast.data.DataUtils
import com.femastudios.fast.data.actions.Action
import com.femastudios.fast.preferences.SettingsActivity
import com.femastudios.fast.views.ActionIconView
import com.femastudios.fast.views.SearchView


class FastActivity : AppCompatActivity() {

    private var resumed = false
    private val query = mutableFieldOf("")
    private val actionsSearcher = ActionSearcher(this, query.async()) {
        if (resumed) {
            it.open()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        setContent.frameLayout {
            background = Utils.makeCubicGradientScrimDrawable(Color.BLACK, 16, Gravity.BOTTOM)
            //Main Layout
            add.linearLayout(true) {
                layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)

                val searchView = SearchView(context) {
                    (actionsSearcher.actions.value as? Loaded)?.value?.firstOrNull()?.open()
                }
                setOnClickListener { searchView.openKeyboard() }

                addView(Toolbar(context).apply {
                    menu.add(R.string.utils_common_settings).apply {
                        setIcon(R.drawable.ic_settings_white_24dp)
                        setOnMenuItemClickListener {
                            startActivity(Intent(this@FastActivity, SettingsActivity::class.java))
                            true
                        }
                        setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
                    }
                    menu.add(R.string.how_to_use).apply {
                        setIcon(R.drawable.ic_help_outline_white_24dp)
                        setOnMenuItemClickListener {
                            openHowToUseDialog(this@FastActivity)
                            true
                        }
                        setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
                    }
                })

                addView(Space(context), LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1f))

                addView(searchView.apply {
                    setTwoWayText(query)
                })
                add.text("") {
                    setText(actionsSearcher.shortcutManager.shortcutInfo.then { it.activeShortcut.hint })
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1f)
                    gravity = Gravity.CENTER_HORIZONTAL
                    setPadding(dp(16))
                    setTextColor(Color.WHITE)
                }
                add.recyclerView(
                    actionsSearcher.actions.valueOr(emptyList()).mapIndexed { index, action ->
                        action to (index == 0)
                    },
                    { (action, _) -> action.visualId },
                    { ActionIconView(context) },
                    { view, (action, large) ->
                        view.setAction(action, large)
                        view.setOnClickListener {
                            action.open(context)
                        }
                    }
                ) {
                    minimumHeight = dp(ActionIconView.MAX_HEIGHT_DP)
                    listen(DataUtils.displayFromRightToLeft) { rtl ->
                        layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, rtl)
                    }
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                }
            }
        }
    }

    fun hideSoftKeyboard() {
        if (currentFocus != null) {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            inputMethodManager?.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }

    private fun Action.open() {
        this.open(this@FastActivity)
        runOnUiThread {
            hideSoftKeyboard()
        }
    }

    override fun onResume() {
        super.onResume()
        resumed = true
        query.value = ""
    }

    override fun onPause() {
        super.onPause()
        resumed = false
    }
}
