/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.preferences

import android.annotation.TargetApi
import android.os.Build
import com.femastudios.fast.R
import com.femastudios.fast.data.shortcuts.YouTubeSearchShortcut

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
class YouTubeSearchShortcutPreferenceFragment : ShortcutPreferenceFragment(
    YouTubeSearchShortcut.prefix,
    "youtube_search_shortcut.prefix",
    R.xml.youtube_search_chortcut_preferences,
    R.string.youtube_search_shortcut_prefix_summary
)