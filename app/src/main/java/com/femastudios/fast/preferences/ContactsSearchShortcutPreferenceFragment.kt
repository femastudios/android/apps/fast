/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.preferences

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import com.femastudios.dataflow.Field
import com.femastudios.dataflow.async.util.async
import com.femastudios.dataflow.extensions.orValue
import com.femastudios.dataflow.listen.FieldListener
import com.femastudios.fast.R
import com.femastudios.fast.data.ContactUtils
import com.femastudios.fast.data.shortcuts.ContactsShortcut

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
class ContactsSearchShortcutPreferenceFragment : ShortcutPreferenceFragment(
    ContactsShortcut.PREFIX.orValue("").async(),
    "contacts_search_shortcut.prefix",
    R.xml.contacts_search_chortcut_preferences,
    R.string.contacts_search_shortcut_prefix_summary
), FieldListener<Boolean> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ContactsShortcut.ENABLED.listeners.addWeakly(this)
    }

    override fun onFieldChanged(source: Field<Boolean>, newData: Boolean) {
        if (newData) {//Contacts are enabled
            val act = activity
            if (act != null) {
                ContactUtils.askPermission(act) {}
            }
        }
    }
}