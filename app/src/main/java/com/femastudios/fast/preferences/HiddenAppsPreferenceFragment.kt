/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.preferences

import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.preference.CheckBoxPreference
import android.preference.PreferenceFragment
import android.view.MenuItem
import com.femastudios.dataflow.Field
import com.femastudios.dataflow.async.Loaded
import com.femastudios.dataflow.async.extensions.mapA
import com.femastudios.dataflow.async.extensions.sortedBy
import com.femastudios.dataflow.async.util.transform
import com.femastudios.dataflow.listen.FieldListener
import com.femastudios.fast.data.DataUtils

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
class HiddenAppsPreferenceFragment : PreferenceFragment(), FieldListener<Any> {

    private val appData = DataUtils.getApps().mapA { app ->
        transform(app.labelWithCompleteName, app.localAppData.thenF { it.getHide() }) { label, hide ->
            Triple(app, label, hide)
        }
    }.sortedBy { it.second }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferenceScreen = preferenceManager.createPreferenceScreen(activity)
        setHasOptionsMenu(true)
        appData.asField().listeners.addWeakly(this)
        reloadHiddenAppsList()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            startActivity(Intent(activity, SettingsActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onFieldChanged(source: Field<Any>, newData: Any) {
        reloadHiddenAppsList()
    }

    private fun reloadHiddenAppsList() {
        val activity = activity
        val preferenceScreen = preferenceScreen
        if (activity != null && preferenceScreen != null) {
            val currentInstalledApps = (appData.value as? Loaded)?.value
            preferenceScreen.removeAll()
            currentInstalledApps?.forEach { app ->
                preferenceScreen.addPreference(CheckBoxPreference(activity).apply {
                    isPersistent = false
                    key = app.first.applicationInfo.packageName
                    title = app.second
                    isChecked = app.third
                    setOnPreferenceChangeListener { _, newHide ->
                        app.first.immediateLocalAppData.setHide(newHide as Boolean)
                        true
                    }
                })
            }
        }
    }
}