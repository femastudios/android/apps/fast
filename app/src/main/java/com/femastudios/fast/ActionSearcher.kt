/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast

import android.app.Activity
import com.femastudios.dataflow.Field
import com.femastudios.dataflow.async.Attribute
import com.femastudios.dataflow.async.AttributeData
import com.femastudios.dataflow.async.Loaded
import com.femastudios.dataflow.async.util.attributeOf
import com.femastudios.dataflow.listen.FieldListener
import com.femastudios.fast.data.DataUtils
import com.femastudios.fast.data.actions.Action
import com.femastudios.fast.data.shortcuts.*

class ActionSearcher(
    activity: Activity,
    val query: Attribute<String>,
    val actionsOpener: (Action) -> Unit
) : FieldListener<AttributeData<SearchResult>> {
    val shortcutManager = ShortcutManager(
        attributeOf(setOf(AppsShortcut, WebSearchShortcut, YouTubeSearchShortcut, ContactsShortcut(activity))),
        query
    )

    private var previouslyProcessedQuery: String? = null

    val queryAndActions = shortcutManager.searchResult
    val actions = queryAndActions.transform { it.actions }

    init {
        queryAndActions.asField().listeners.addWeakly(this)
    }

    override fun onFieldChanged(source: Field<AttributeData<SearchResult>>, newData: AttributeData<SearchResult>) {
        val value = queryAndActions.value
        if (value is Loaded) {
            val query = value.value
            val previousQuery = previouslyProcessedQuery
            //Auto open enable
            if (DataUtils.automaticallyOpenApps.value) {
                //I've added more characters
                if (previousQuery == null || (query.query.length > previousQuery.length && query.query.startsWith(previousQuery))) {
                    if (query.shortcutInfo.isDefinitive && query.actions.count { it.allowAutomaticOpen } == 1 && query.actions.firstOrNull()?.allowAutomaticOpen == true) {
                        actionsOpener(query.actions.first())
                    }
                }
            }
            previouslyProcessedQuery = query.query
        }
    }
}