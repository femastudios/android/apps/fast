/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteStatement
import androidx.core.database.getLongOrNull
import androidx.core.database.getStringOrNull
import com.femastudios.dataflow.DataflowThreadUtils
import com.femastudios.dataflow.Field
import com.femastudios.dataflow.async.Attribute
import com.femastudios.dataflow.async.AttributeData
import com.femastudios.dataflow.async.Loaded
import com.femastudios.dataflow.async.util.attributeWrapperOf
import com.femastudios.dataflow.util.mutableFieldOf
import org.threeten.bp.Instant


class LocalAppData private constructor(
    val packageName: String
) {
    private val openCount = mutableFieldOf(0L)
    private val priority = mutableFieldOf(0L)
    private val lastOpen = mutableFieldOf(null as Instant?)
    private val hide = mutableFieldOf(false)
    private val alternativeName = mutableFieldOf(null as String?)

    fun getOpenCount(): Field<Long> = openCount
    fun getPriority(): Field<Long> = priority
    fun getLastOpen(): Field<Instant?> = lastOpen
    fun getHide(): Field<Boolean> = hide
    fun getAlternativeName(): Field<String?> = alternativeName

    fun setHasBeenOpened() {
        openCount.value = openCount.value + 1
        lastOpen.value = Instant.now()
        save()
    }

    fun setPriority(priority: Long) {
        this.priority.value = priority
        save()
    }

    fun setHide(hide: Boolean) {
        this.hide.value = hide
        save()
    }

    fun setAlternativeName(alternativeName: String?) {
        this.alternativeName.value = alternativeName
        save()
    }

    private fun save() {
        LocalAppDataModel.save(FastDB.writableDatabase, this)
    }

    companion object {
        private val cache = HashMap<String, LocalAppData>()
        private var loaded = false

        val appData = attributeWrapperOf<Map<String, LocalAppData>>(AttributeData.loadingInstance())

        fun getInstance(packageName: String): Attribute<LocalAppData> {
            if (!loaded) {
                synchronized(cache) {
                    if (!loaded) {
                        loaded = true
                        DataflowThreadUtils.execute {
                            LocalAppDataModel.loadAll(FastDB.readableDatabase)
                            appData.setLoadedValue(cache)
                        }
                    }
                }
            }
            return appData.transform {
                it[packageName] ?: _getInstance(packageName)
            }
        }

        fun getInstanceImmediately(packageName: String): LocalAppData {
            return (getInstance(packageName).waitWhileLoading() as Loaded).value
        }

        private fun _getInstance(packageName: String): LocalAppData {
            synchronized(cache) {
                return cache.getOrPut(packageName) { LocalAppData(packageName) }
            }
        }
    }

    object LocalAppDataModel {

        fun load(c: Cursor): LocalAppData {
            val packageName = c.getString(c.getColumnIndex("packageName"))!!
            val openCount = c.getLong(c.getColumnIndex("openCount"))
            val priority = c.getLong(c.getColumnIndex("priority"))
            val lastOpen = c.getLongOrNull(c.getColumnIndex("lastOpen"))?.let { Instant.ofEpochMilli(it) }
            val hide = c.getInt(c.getColumnIndex("hide")) == 1
            val alternativeName = c.getStringOrNull(c.getColumnIndex("alternativeName"))

            return _getInstance(packageName).also { app ->
                app.openCount.value = openCount
                app.priority.value = priority
                app.lastOpen.value = lastOpen
                app.hide.value = hide
                app.alternativeName.value = alternativeName
            }
        }


        fun save(db: SQLiteDatabase, vararg bindings: LocalAppData) {
            save(db, bindings.asList())
        }

        fun save(db: SQLiteDatabase, bindings: List<LocalAppData>) {
            createSaveStatement(db).use { stmt ->
                bindings.forEach {
                    bindParamsToSaveStatement(stmt, it)
                    stmt.execute()
                }
            }
        }

        fun createSaveStatement(db: SQLiteDatabase): SQLiteStatement {
            return db.compileStatement("INSERT OR REPLACE INTO App(packageName, openCount, priority, lastOpen, hide, alternativeName) VALUES (?,?,?,?,?,?)")
        }

        fun bindParamsToSaveStatement(stmt: SQLiteStatement, localAppData: LocalAppData) {
            stmt.bindString(1, localAppData.packageName)
            stmt.bindLong(2, localAppData.openCount.value)
            stmt.bindLong(3, localAppData.priority.value)
            val lastOpen = localAppData.lastOpen.value
            if (lastOpen == null) {
                stmt.bindNull(4)
            } else {
                stmt.bindLong(4, lastOpen.toEpochMilli())
            }
            stmt.bindLong(5, if (localAppData.hide.value) 1 else 0)
            val alternativeName = localAppData.alternativeName.value
            if (alternativeName == null) {
                stmt.bindNull(6)
            } else {
                stmt.bindString(6, alternativeName)
            }
        }

        fun loadAll(db: SQLiteDatabase): List<LocalAppData> {
            db.rawQuery(
                "SELECT packageName, openCount, priority, lastOpen, hide, alternativeName FROM App",
                null
            ).use { c ->
                return mutableListOf<LocalAppData>().apply {
                    while (c.moveToNext()) {
                        add(load(c))
                    }
                }
            }
        }
    }
}