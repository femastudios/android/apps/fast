/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data

import android.content.res.AssetFileDescriptor
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import com.femastudios.dataflow.imageloader.ImageLoaderManager
import com.femastudios.dataflow.imageloader.model.*
import com.femastudios.fast.FastApplication
import java.io.FileNotFoundException
import java.io.IOException

class AppIconResource(val app: App) : Resource() {
    override val id: String = app.applicationInfo.packageName
}

object AppIconLoader : ResourceLoader(false, true) {

    override fun createJob(): Job {
        return object : Job() {
            @Throws(LoadException::class)
            override fun doLoad(imageLoaderManager: ImageLoaderManager, imageInfo: ImageInfo): Result {
                val resource = imageInfo.resource as AppIconResource
                return Result(
                    imageInfo,
                    resource.app.applicationInfo.loadIcon(resource.app.packageManager)
                )
            }
        };
    }

    override fun getAvailable(resources: List<Resource>, availability: MutableList<ImageInfo>, requests: Request) {
        for (resource in resources) {
            if (resource is AppIconResource) {
                availability.add(ImageInfo(requests.image, 1, resource, null))
            }
        }
    }
}

class ContactPhotoResource(val photoUri: String) : Resource() {
    override val id: String = "avatar://$photoUri"
}

object ContactPhotoLoader : ResourceLoader(false, true) {

    override fun createJob(): Job {
        return object : Job() {
            @Throws(LoadException::class)
            override fun doLoad(imageLoaderManager: ImageLoaderManager, imageInfo: ImageInfo): Result {
                val resource = imageInfo.resource as ContactPhotoResource
                val thumbUri = Uri.parse(resource.photoUri)

                var afd: AssetFileDescriptor? = null
                val bitmap = try {
                    afd = FastApplication.instance.contentResolver?.openAssetFileDescriptor(thumbUri, "r")
                    val fileDescriptor = afd?.fileDescriptor ?: throw LoadException("Cannot get file descriptor")
                    BitmapFactory.decodeFileDescriptor(fileDescriptor, null, null)
                } catch (e: FileNotFoundException) {
                    throw LoadException(e)
                } finally {
                    // In all cases, close the asset file descriptor
                    try {
                        afd?.close()
                    } catch (e: IOException) {
                    }
                }


                return Result(
                    imageInfo,
                    BitmapDrawable(FastApplication.instance.resources, bitmap)
                )
            }
        };
    }

    override fun getAvailable(resources: List<Resource>, availability: MutableList<ImageInfo>, requests: Request) {
        for (resource in resources) {
            if (resource is ContactPhotoResource) {
                availability.add(ImageInfo(requests.image, 1, resource, null))
            }
        }
    }
}