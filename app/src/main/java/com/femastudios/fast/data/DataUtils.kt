/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data

import android.preference.PreferenceManager
import com.femastudios.dataflow.android.preferences.getBooleanField
import com.femastudios.dataflow.async.Attribute
import com.femastudios.dataflow.async.extensions.filterNotA
import com.femastudios.dataflow.async.extensions.mapA
import com.femastudios.dataflow.async.extensions.sortedByA
import com.femastudios.dataflow.async.extensions.sortedByDescendingA
import com.femastudios.dataflow.async.util.attributeOf
import com.femastudios.fast.FastApplication
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import java.util.*


object DataUtils {

    private val APPS = attributeOf {
        computeInstalledApps()
    }

    fun getApps(): Attribute<List<App>> = APPS

    val notHiddenApps: Attribute<List<App>> = APPS.filterNotA { app -> app.localAppData.thenF { it.getHide() } }

    val sortedNotHiddenApps: Attribute<List<App>> = notHiddenApps
        .sortedByA { it.name.toStringA() }
        .sortedByDescendingA { app -> app.localAppData.thenF { it.getLastOpen() } }
        .sortedByDescendingA { app -> app.localAppData.thenF { it.getOpenCount() } }
        .sortedByDescendingA { app ->
            app.localAppData
                .thenF { it.getLastOpen() }
                .transform { it?.isAfter(Instant.now() - Duration.ofMinutes(15)) ?: false }
        }
        .sortedByDescendingA { app -> app.localAppData.thenF { it.getPriority() } }

    val sortedNotHiddenAppsWithTokens = sortedNotHiddenApps.mapA { a -> a.nameTokens.transform { a to it } }

    val displayFromRightToLeft = PreferenceManager.getDefaultSharedPreferences(FastApplication.instance).getBooleanField("display_apps_right_to_left", false)
    val automaticallyOpenApps = PreferenceManager.getDefaultSharedPreferences(FastApplication.instance).getBooleanField("automatically_open_apps", true)

    fun reloadInstalledApps() {
        APPS.recompute()
    }

    private fun computeInstalledApps(): List<App> {
        val pm = FastApplication.instance.packageManager
        return pm.getInstalledApplications(0)
            ?.filterNot { it.packageName == FastApplication.instance.packageName }
            ?.filter { pm.getLaunchIntentForPackage(it.packageName) != null }
            ?.map { App(pm, it) }
            ?: Collections.emptyList()
    }
}

