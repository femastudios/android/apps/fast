/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data.shortcuts

import com.femastudios.dataflow.async.Attribute
import com.femastudios.dataflow.async.util.attributeOf
import com.femastudios.fast.R
import com.femastudios.fast.Utils
import com.femastudios.fast.data.DataUtils
import com.femastudios.fast.data.actions.Action
import com.femastudios.fast.data.actions.WebSearchAction
import com.femastudios.fast.data.actions.YouTubeSearchAction
import com.femastudios.fast.strRes

object AppsShortcut : Shortcut {
    override val enabled = attributeOf(true)
    override val prefix = attributeOf("")
    override val hint = attributeOf(R.string.type_name_of_app_to_open.strRes())

    override fun search(query: Attribute<String>): Attribute<Pair<String, List<Action>>> {
        return query.then { q ->
            DataUtils.sortedNotHiddenAppsWithTokens.transform { apps ->
                val actions = apps
                    .asSequence()
                    .filter {
                        Utils.tokensMatchQuery(it.second, q)
                    }
                    .mapTo(ArrayList<Action>()) { it.first }
                if (q.isNotEmpty()) {
                    actions.add(WebSearchAction(q))
                    actions.add(YouTubeSearchAction(q))
                }
                q to (actions as List<Action>)
            }
        }
    }
}