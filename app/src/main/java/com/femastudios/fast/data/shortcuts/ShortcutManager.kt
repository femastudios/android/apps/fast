/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data.shortcuts

import com.femastudios.dataflow.async.Attribute
import com.femastudios.dataflow.async.extensions.filterA
import com.femastudios.dataflow.async.extensions.mapA
import com.femastudios.dataflow.async.extensions.sortedByDescending
import com.femastudios.dataflow.async.util.transform
import com.femastudios.fast.data.actions.Action

class ShortcutManager(
    val shortcuts: Attribute<Set<Shortcut>>,
    val query: Attribute<String>
) {
    private val shortcutsByPrefixLength = shortcuts
        .filterA { it.enabled }
        .mapA { s -> s.prefix.transform { s to it } }
        .sortedByDescending { it.second }

    private val nnQuery = query.transform { it }

    val shortcutInfo = transform(nnQuery, shortcutsByPrefixLength) { query, shortcuts ->
        val activeShortcut = shortcuts.first { shortcut ->
            shortcut.second.isEmpty() || query.startsWith(shortcut.second + " ")
        }
        val isDefinitive = shortcuts.none { shortcut ->
            shortcut.second.startsWith(query)
        }
        ShortcutInfo(activeShortcut.first, activeShortcut.second, isDefinitive)
    }

    val searchResult = shortcutInfo.then { sInfo ->
        val queryWithNoPrefix = nnQuery.transform { it.removePrefix(sInfo.activeShortcutPrefix).trim() }
        sInfo.activeShortcut.search(queryWithNoPrefix).transform {
            SearchResult(it.first, it.second, sInfo)
        }
    }
}

data class ShortcutInfo(
    val activeShortcut: Shortcut,
    val activeShortcutPrefix: String,
    val isDefinitive: Boolean
)

data class SearchResult(
    val query: String,
    val actions: List<Action>,
    val shortcutInfo: ShortcutInfo
)