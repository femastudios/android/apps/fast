/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data.actions

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import com.femastudios.dataflow.async.util.attributeOf
import com.femastudios.dataflow.imageloader.implementations.resources.LocalResourceResource
import com.femastudios.dataflow.imageloader.model.Image
import com.femastudios.dataflow.imageloader.model.Transformation
import com.femastudios.fast.FastApplication
import com.femastudios.fast.R
import com.femastudios.fast.strRes
import java.net.URLEncoder


private val searchName = attributeOf(R.string.youtube_search.strRes())

data class YouTubeSearchAction(val query: String) : Action {

    override val name = searchName
    override val visualId = YouTubeSearchAction::class.java.hashCode() * 31L
    override val icon = Image.of(LocalResourceResource(FastApplication.instance.resources, R.drawable.ic_yt_icon_mono_dark_96dp))
    override val imageTransformation: Transformation? = null
    override val allowAutomaticOpen = false

    override fun open(context: Context) {
        try {
            val intent = Intent(Intent.ACTION_SEARCH)
            intent.setPackage("com.google.android.youtube")
            intent.putExtra("query", query)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            val escapedQuery = URLEncoder.encode(query, "UTF-8")
            val uri = Uri.parse("https://www.youtube.com/results?search_query=$escapedQuery")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            context.startActivity(intent)
        }
    }

    override fun openOptionsDialog(context: Context) {
    }
}