/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data

import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.femastudios.fast.FastApplication

object FastDB : SQLiteOpenHelper(FastApplication.instance, "FastDB", null, 3) {

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.execSQL(
            "CREATE TABLE App(" +
                    "packageName TEXT NOT NULL PRIMARY KEY, " +
                    "openCount INTEGER NOT NULL DEFAULT 0," +
                    "priority INTEGER NOT NULL DEFAULT 0," +
                    "lastOpen INTEGER DEFAULT NULL," +
                    "hide INTEGER NOT NULL DEFAULT 0," +
                    "alternativeName TEXT DEFAULT NULL" +
                    ")"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (oldVersion < 3) {
            db.execSQL("ALTER TABLE App ADD priority INTEGER NOT NULL DEFAULT 0")
        }
    }

}
