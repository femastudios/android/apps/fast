/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data.shortcuts

import android.preference.PreferenceManager
import com.femastudios.dataflow.android.preferences.getBooleanField
import com.femastudios.dataflow.android.preferences.getStringField
import com.femastudios.dataflow.async.Attribute
import com.femastudios.dataflow.async.util.async
import com.femastudios.dataflow.async.util.attributeOf
import com.femastudios.dataflow.extensions.orValue
import com.femastudios.fast.FastApplication
import com.femastudios.fast.R
import com.femastudios.fast.data.actions.Action
import com.femastudios.fast.data.actions.YouTubeSearchAction
import com.femastudios.fast.strRes

object YouTubeSearchShortcut : Shortcut {
    override val enabled = PreferenceManager.getDefaultSharedPreferences(FastApplication.instance).getBooleanField("youtube_search_shortcut.enabled", true).async()
    override val prefix = PreferenceManager.getDefaultSharedPreferences(FastApplication.instance).getStringField("youtube_search_shortcut.prefix", "y").orValue("").async()
    override val hint = attributeOf {
        R.string.search_on_youtube.strRes()
    }

    override fun search(query: Attribute<String>): Attribute<Pair<String, List<Action>>> {
        return query.transform { q ->
            q to listOf(YouTubeSearchAction(q))
        }
    }
}