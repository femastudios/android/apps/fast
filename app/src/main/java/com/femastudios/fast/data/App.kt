/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import com.femastudios.android.core.dp
import com.femastudios.android.core.setPadding
import com.femastudios.dataflow.DataflowThreadUtils
import com.femastudios.dataflow.android.FieldAlertDialogBuilder
import com.femastudios.dataflow.android.viewState.extensions.setHint
import com.femastudios.dataflow.android.viewState.extensions.setVisible
import com.femastudios.dataflow.async.Attribute
import com.femastudios.dataflow.async.RecomputableAttribute
import com.femastudios.dataflow.async.extensions.valueOr
import com.femastudios.dataflow.async.util.attributeOf
import com.femastudios.dataflow.async.util.transform
import com.femastudios.dataflow.imageloader.model.Transformation
import com.femastudios.fast.R
import com.femastudios.fast.Utils
import com.femastudios.fast.data.actions.Action
import com.femastudios.fast.setText
import com.femastudios.fast.strRes


data class App(
    val packageManager: PackageManager,
    val applicationInfo: ApplicationInfo
) : Action {
    override val imageTransformation: Transformation? = null//TODO: trim

    override val allowAutomaticOpen = true

    override val visualId = App::class.java.hashCode() * 31 + applicationInfo.packageName.hashCode().toLong()
    val localAppData: Attribute<LocalAppData> = LocalAppData.getInstance(applicationInfo.packageName)
    val label: RecomputableAttribute<CharSequence> = attributeOf {
        applicationInfo.loadLabel(packageManager)
    }
    val immediateLocalAppData by lazy {
        LocalAppData.getInstanceImmediately(applicationInfo.packageName)
    }

    override val name = transform(label, localAppData.thenF { it.getAlternativeName() }) { l, an ->
        an ?: l
    }
    val labelWithCompleteName = transform(label, localAppData.thenF { it.getAlternativeName() }) { l, an ->
        if (an == null) {
            l.toString()
        } else {
            "$an ($l)";
        }
    }
    val nameTokens = name.transform {
        Utils.tokenize(it.toString())
    }
    override val icon = AppIconResource(this).toImage()

    val openIntent: Intent = packageManager.getLaunchIntentForPackage(applicationInfo.packageName)!!
        .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

    override fun open(context: Context) {
        context.startActivity(openIntent)
        DataflowThreadUtils.execute {
            immediateLocalAppData.setHasBeenOpened()
        }
    }

    fun openRenameDialog(context: Context) {
        FieldAlertDialogBuilder(context)
            .setTitle(name.valueOr(""))
            .create().apply {
                val editText = EditText(context)
                setView(editText.apply {
                    inputType = EditorInfo.TYPE_CLASS_TEXT
                    setPadding(dp(32))
                    setText(name)
                    setHint(label.valueOrNull())
                    selectAll()
                    requestFocus()
                    post { requestFocus() }
                })
                setButton(DialogInterface.BUTTON_NEGATIVE, R.string.restore_name.strRes()) { _, _ ->
                    immediateLocalAppData.setAlternativeName(null)
                }
                setButton(DialogInterface.BUTTON_POSITIVE, android.R.string.ok.strRes()) { _, _ ->
                    val text = editText.text?.trim()
                    immediateLocalAppData.setAlternativeName(if (text == null || text.isEmpty()) null else text.toString())
                }
                setOnShowListener {
                    getButton(DialogInterface.BUTTON_NEGATIVE).setVisible(localAppData.thenF { it.getAlternativeName() }.valueOrNull().isNotNull())
                }
                show()
            }
    }

    fun openPriorityDialog(context: Context) {
        FieldAlertDialogBuilder(context)
            .setTitle(R.string.change_priority)
            .setSingleChoiceItems(
                R.array.priorities, when (immediateLocalAppData.getPriority().value) {
                    -1L -> 3
                    0L -> 2
                    1L -> 1
                    2L -> 0
                    else -> throw IllegalStateException()
                }
            ) { _, which ->
                immediateLocalAppData.setPriority(
                    when (which) {
                        0 -> 2
                        1 -> 1
                        2 -> 0
                        3 -> -1
                        else -> throw IllegalStateException()
                    }
                )
            }
            .setPositiveButton(android.R.string.ok, null)
            .show()
    }

    override fun openOptionsDialog(context: Context) {
        FieldAlertDialogBuilder(context)
            .setTitle(name.valueOr(""))
            .setItems(R.array.app_options) { _, which ->
                when (which) {
                    0 -> immediateLocalAppData.setHide(true)
                    1 -> openRenameDialog(context)
                    2 -> openPriorityDialog(context)
                    else -> throw IllegalStateException()
                }

            }
            .show()
    }
}