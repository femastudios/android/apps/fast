/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data

import android.Manifest
import android.app.Activity
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.femastudios.fast.R
import com.femastudios.fast.data.shortcuts.ContactsShortcut
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.BasePermissionListener

object ContactUtils {

    fun askPermission(activity: Activity, onGranted: () -> Unit) {
        Dexter.withActivity(activity)
            .withPermission(Manifest.permission.READ_CONTACTS)
            .withListener(object : BasePermissionListener() {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    onGranted()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    ContactsShortcut.ENABLED.rawValue.value = false
                    Toast.makeText(activity, R.string.contact_permission_denied, Toast.LENGTH_SHORT).show()
                }

                override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
                    AlertDialog.Builder(activity)
                        .setMessage(R.string.contact_permission_explanation)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            token.continuePermissionRequest()
                        }
                        .setNeutralButton(android.R.string.cancel) { _, _ ->
                            token.cancelPermissionRequest()
                        }
                        .show()
                }
            })
            .check()
    }
}