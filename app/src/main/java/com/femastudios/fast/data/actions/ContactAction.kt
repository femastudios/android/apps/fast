/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data.actions

import android.content.Context
import android.content.Intent
import android.provider.ContactsContract
import com.femastudios.dataflow.async.util.attributeOf
import com.femastudios.dataflow.imageloader.implementations.resources.LocalResourceResource
import com.femastudios.dataflow.imageloader.implementations.transformations.CropCircleTransformation
import com.femastudios.dataflow.imageloader.implementations.transformations.IgnoreTransformation
import com.femastudios.dataflow.imageloader.model.Image
import com.femastudios.fast.FastApplication
import com.femastudios.fast.R
import com.femastudios.fast.data.ContactPhotoResource


data class ContactAction(
    val id: Long,
    val key: String,
    val namePrimary: String,
    val photoUri: String?
) : Action {
    override val name = attributeOf(namePrimary)
    override val visualId: Long = ContactAction::class.java.hashCode() * 31 + id
    override val icon = if (photoUri != null) {
        Image.of(ContactPhotoResource(photoUri))
    } else {
        Image.of(LocalResourceResource(FastApplication.instance.resources, R.drawable.ic_person_white_96dp))
    }
    override val imageTransformation = if (photoUri != null) {
        IgnoreTransformation(CropCircleTransformation())
    } else {
        null
    }

    override val allowAutomaticOpen = true

    override fun open(context: Context) {
        val contactUri = ContactsContract.Contacts.getLookupUri(id, key)
        val intent = Intent(Intent.ACTION_VIEW, contactUri)
        context.startActivity(intent)
    }

    override fun openOptionsDialog(context: Context) {
    }
}