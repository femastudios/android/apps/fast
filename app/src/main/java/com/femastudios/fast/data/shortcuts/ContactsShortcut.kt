/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.data.shortcuts

import android.app.Activity
import android.database.Cursor
import android.preference.PreferenceManager
import android.provider.ContactsContract
import com.femastudios.dataflow.android.preferences.getBooleanField
import com.femastudios.dataflow.android.preferences.getStringField
import com.femastudios.dataflow.async.Attribute
import com.femastudios.dataflow.async.util.async
import com.femastudios.dataflow.async.util.attributeOf
import com.femastudios.dataflow.extensions.orValue
import com.femastudios.fast.FastApplication
import com.femastudios.fast.R
import com.femastudios.fast.data.ContactUtils
import com.femastudios.fast.data.actions.Action
import com.femastudios.fast.data.actions.ContactAction
import com.femastudios.fast.strRes


class ContactsShortcut(
    val activity: Activity
) : Shortcut {

    override val enabled = ENABLED.async()
    override val prefix = PREFIX.orValue("").async()
    override val hint = attributeOf {
        R.string.search_on_contacts.strRes()
    }

    override fun search(query: Attribute<String>): Attribute<Pair<String, List<Action>>> {
        val ret = query.transform { q ->
            val contactCursor = getListOfContactNames(q) ?: throwError()
            val actions: List<Action> = contactCursor.use { c ->
                mutableListOf<Action>().apply {
                    while (c.moveToNext()) {
                        val contactId = c.getLong(0)
                        val contactKey = c.getString(1)
                        val name = c.getString(2)
                        val photoUri = c.getString(3)
                        add(ContactAction(contactId, contactKey, name, photoUri))
                    }
                }
            }
            q to actions
        }
        ContactUtils.askPermission(activity) {
            ret.recompute()
        }
        return ret
    }

    private fun getListOfContactNames(searchText: String): Cursor? {
        val cr = FastApplication.instance.contentResolver
        val mProjection = arrayOf(ContactsContract.Contacts._ID, ContactsContract.Contacts.LOOKUP_KEY, ContactsContract.Contacts.DISPLAY_NAME_PRIMARY, ContactsContract.Contacts.PHOTO_THUMBNAIL_URI)
        val uri = ContactsContract.Contacts.CONTENT_URI
        val selection = ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " LIKE ?"
        val selectionArgs = arrayOf("%$searchText%")
        return try {
            cr.query(uri, mProjection, selection, selectionArgs, null)
        } catch (e: SecurityException) {
            null
        }
    }

    companion object {
        val ENABLED = PreferenceManager.getDefaultSharedPreferences(FastApplication.instance).getBooleanField("contacts_search_shortcut.enabled", false)
        val PREFIX = PreferenceManager.getDefaultSharedPreferences(FastApplication.instance).getStringField("contacts_search_shortcut.prefix", "c")
    }
}