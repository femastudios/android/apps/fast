/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.graphics.drawable.Drawable
import android.graphics.drawable.PaintDrawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RectShape
import android.view.Gravity
import android.widget.LinearLayout
import androidx.annotation.FloatRange
import androidx.appcompat.app.AlertDialog
import com.femastudios.android.core.dp
import com.femastudios.android.core.setPadding
import com.femastudios.dataflow.android.declarativeui.add
import kotlin.math.max
import kotlin.math.pow

object Utils {

    fun tokenize(appName: String): List<String> {
        val tokens = mutableListOf<String>()
        var prev = 0
        for (i in 1 until appName.length) {
            val prevChar = appName[i - 1]
            val nextChar = appName.getOrNull(i + 1)
            val char = appName[i]
            if (char.isWhitespace() ||
                (!prevChar.isWhitespace() && prevChar.isLowerCase() && char.isUpperCase()) ||
                (nextChar != null && !nextChar.isWhitespace() && char.isUpperCase() && nextChar.isLowerCase())
            ) {
                tokens.add(appName.substring(prev, i))
                prev = i
            }
        }
        tokens.add(appName.substring(prev))
        return tokens.map { it.trim() }.filter { it.isNotEmpty() }
    }

    fun tokensMatchQuery(tokens: List<String>, query: String): Boolean {
        val queryTokenized = tokenize(query).asSequence().flatMap { it.asSequence() }

        return tokens
            .asSequence()
            .map { it[0] }
            .plus(generateSequence { ' ' })
            .zip(queryTokenized.asSequence())
            .all { (q, t) -> q.toLowerCase() == t.toLowerCase() }//Initials match
                || tokens
            .asSequence()
            .windowed(size = query.length + 1, partialWindows = true)
            .any { partialTokens ->
                partialTokens
                    .asSequence()
                    .flatMap { it.asSequence() }
                    .plus(generateSequence { ' ' })
                    .zip(queryTokenized.asSequence())
                    .all { (q, t) -> q.toLowerCase() == t.toLowerCase() }
            }//Normal search
    }

    fun openHowToUseDialog(context: Context) {
        AlertDialog.Builder(context)
            .setView(LinearLayout(context).apply {
                orientation = LinearLayout.VERTICAL
                setPadding(dp(24))
                add.text(R.string.how_to_use_explanation.strRes())
                for (example in resources.getStringArray(R.array.how_to_use_examples)) {
                    add.linearLayout {
                        add.text("•") {
                            minWidth = dp(8)
                        }
                        add.text(example)
                    }
                }
            })
            .setPositiveButton(android.R.string.ok, null)
            .show()
    }

    /*
    * Creates an approximated cubic gradient using a multi-stop linear gradient.
    * See (this post)[https://plus.google.com/+RomanNurik/posts/2QvHVFWrHZf"] for more details.
    */
    @SuppressLint("RtlHardcoded")
    fun makeCubicGradientScrimDrawable(baseColor: Int, numStops: Int, gravity: Int, @FloatRange(from = 0.0, to = 1.0) targetAlpha: Float = 0f): Drawable {
        val realNumStops = max(numStops, 2);
        val stopColors = IntArray(realNumStops)

        val red = Color.red(baseColor);
        val green = Color.green(baseColor);
        val blue = Color.blue(baseColor);
        val alpha = Color.alpha(baseColor);

        for (i in 0 until realNumStops) {
            val x = i * 1f / (numStops - 1);
            val opacity = (targetAlpha + (1 - targetAlpha) * x.pow(3)).coerceIn(0f, 1f)
            stopColors[i] = Color.argb((alpha * opacity).toInt(), red, green, blue);
        }

        val x0: Float
        val x1: Float
        val y0: Float
        val y1: Float
        when (gravity and Gravity.HORIZONTAL_GRAVITY_MASK) {
            Gravity.LEFT -> {
                x0 = 1f
                x1 = 0f
            }
            Gravity.RIGHT -> {
                x0 = 0f
                x1 = 1f
            }
            else -> {
                x0 = 0f
                x1 = 0f
            }
        }
        when (gravity and Gravity.VERTICAL_GRAVITY_MASK) {
            Gravity.TOP -> {
                y0 = 1f
                y1 = 0f
            }
            Gravity.BOTTOM -> {
                y0 = 0f
                y1 = 1f
            }
            else -> {
                y0 = 0f
                y1 = 0f
            }
        }
        return ShaderWrapper { w, h ->
            LinearGradient(w * x0, h * y0, w * x1, h * y1, stopColors, null, Shader.TileMode.CLAMP)
        }
    }

    private class ShaderWrapper(shaderCreator: (w: Int, h: Int) -> Shader) : PaintDrawable() {
        init {
            shape = RectShape();
            shaderFactory = object : ShapeDrawable.ShaderFactory() {
                private var oldW: Int = 0
                private var oldH: Int = 0
                private var oldShader: Shader? = null
                override fun resize(width: Int, height: Int): Shader {
                    return if (oldShader == null || width != oldW || oldH != height) {
                        val shader = shaderCreator(width, height)
                        oldW = width
                        oldH = height
                        oldShader = shader
                        shader
                    } else {
                        oldShader!!
                    }
                }
            }
        }
    }
}

fun Int.strRes(): String = FastApplication.instance.getString(this)
fun Int.strRes(vararg args: Any?): String = FastApplication.instance.getString(this, *args)

fun <K, V> Map<K, V>.getNN(key: K) = getValue(key)