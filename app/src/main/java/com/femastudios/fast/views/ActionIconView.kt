/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.views

import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Space
import android.widget.TextView
import com.femastudios.android.core.dp
import com.femastudios.android.core.setPadding
import com.femastudios.android.core.setPaddingBottom
import com.femastudios.android.core.setPaddingHorizontal
import com.femastudios.dataflow.util.fieldWrapperOf
import com.femastudios.fast.R
import com.femastudios.fast.data.actions.Action
import com.femastudios.fast.setImage
import com.femastudios.fast.setText

class ActionIconView(context: Context) : LinearLayout(context) {

    private val icon = ImageView(context)
    private val label = TextView(context)
    val large = fieldWrapperOf(false)

    init {
        gravity = Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM
        orientation = VERTICAL
        //TODO setBackgroundResource(R.drawable.item_background_circular_dark)

        addView(Space(context), LayoutParams(LayoutParams.MATCH_PARENT, 0, 1f))
        addView(icon.apply {
            setPadding(dp(4))
            scaleType = ImageView.ScaleType.FIT_CENTER
        })
        addView(Space(context), LayoutParams(LayoutParams.MATCH_PARENT, 0, 1f))

        addView(label.apply {
            setTextColor(Color.WHITE)
            gravity = Gravity.CENTER_HORIZONTAL
            ellipsize = TextUtils.TruncateAt.END
            maxLines = 1
            textSize = 12f
            setPaddingBottom(dp(8))
            setPaddingHorizontal(dp(1))
            minWidth = dp(72)

        })
    }

    fun setAction(action: Action, large: Boolean) {
        icon.setImage(action.icon, action.imageTransformation)
        label.setText(action.name)
        setOnLongClickListener {
            action.openOptionsDialog(context)
            true
        }
        icon.layoutParams = if (large) {
            LayoutParams(dp(72), dp(72))
        } else {
            LayoutParams(dp(56), dp(56))
        }

    }

    companion object {
        const val MAX_HEIGHT_DP = 96
    }
}