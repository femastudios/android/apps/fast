/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast.views

import android.content.Context
import android.graphics.Color
import android.text.InputType
import android.view.Gravity
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.AppCompatEditText


const val MAX_SIZE_SP = 96

class SearchView(context: Context, val onGoPressed: () -> Unit) : AppCompatEditText(context) {
    init {
        gravity = Gravity.CENTER
        setTextColor(Color.WHITE)
        background = null
        maxLines = 1
        textSize = MAX_SIZE_SP.toFloat()
        //minHeight = DP16 + MetricsUtils.spToPx(MAX_SIZE_SP)

        //Submit
        inputType = EditorInfo.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
        imeOptions = EditorInfo.IME_ACTION_GO
        setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                onGoPressed()
                true
            } else {
                false
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        openKeyboard()
    }

    fun openKeyboard() {
        requestFocus()
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.toggleSoftInputFromWindow(
            this.applicationWindowToken,
            InputMethodManager.SHOW_FORCED, 0
        )
    }

    /*override fun onKeyPreIme(keyCode: Int, event: KeyEvent?): Boolean {
        //Prevent the keyboard from closing
        return true;
    }*/
}