/*
 * This file is part of Fast.
 *
 * Fast is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.femastudios.fast

import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import android.widget.TextView
import com.femastudios.dataflow.android.viewState.extensions.setTextViewState
import com.femastudios.dataflow.android.viewState.states.backgroundColor
import com.femastudios.dataflow.android.viewState.states.text
import com.femastudios.dataflow.async.Attribute
import com.femastudios.dataflow.async.android.toViewState
import com.femastudios.dataflow.async.util.attributeOf
import com.femastudios.dataflow.async.util.attributeOfNull
import com.femastudios.dataflow.imageloader.implementations.resources.LocalResourceResource
import com.femastudios.dataflow.imageloader.model.Image
import com.femastudios.dataflow.imageloader.model.Transformation
import com.femastudios.dataflow.imageloader.utils.setImage

private const val STATE_LOADING_COLOR = 0x208c8c8c
private const val STATE_ERROR_COLOR = 0x20ff006a


fun TextView.setText(text: Attribute<CharSequence?>) {
    setTextViewState(text.toViewState(
        fixedStates = null,
        onLoading = { backgroundColor(STATE_LOADING_COLOR) },
        onError = { backgroundColor(STATE_ERROR_COLOR) },
        onLoaded = { text(it) }
    ))
}

fun ImageView.setImage(image: Image?, transformation: Transformation? = null) = setImage(attributeOf(image), attributeOf(transformation))
fun ImageView.setImage(image: Attribute<Image?>, transformation: Attribute<Transformation?> = attributeOfNull()) {
    setImage(
        image,
        transformation,
        errorImage = attributeOf(LocalResourceResource(ColorDrawable(STATE_ERROR_COLOR)).toImage())
    )
}
