# Fast
Fast is a completely free app without ads that let you switch between apps very quickly.  
Once you've got used to Fast, you can open any app on your phone in less than one second, no matter what you're doing.

The main idea is very simple: type the name of the app, and as soon as what you've typed has a unique matching, the app opens.

You can download it from the [Play Store](https://play.google.com/store/apps/details?id=com.femastudios.fast).

Let's suppose you want to open the app [MoviesFad](https://play.google.com/store/apps/details?id=fema.moviesfad). You can open Fast using a shortcut, then type the initials of the app, for example "Mo". As soon as there's a unique app that matches what you've written, MoviesFad opens.

Obviously, to use Fast, you'll need to open it first. How you do that it's up to you, but I've got a few suggestions:

* If your phone supports it, you can choose to open Fast when you press a physical button. For example, the new S10 supports remapping the Bixbi button
* If you use Nove launcher (or basically any other launcher), you can choose to open Fast when you press the home button.
* If you use Fluid Navigation Gestures, you can choose to open Fast by swiping from the edges of your screen.

## Nice things about Fast

* Completely free
* No ads
* No internet permission (no data will leave your device)
* Under 2.5 MB

## How to compile
The app itself and all the content of this repository is licenced under a GNU GPLv3 licence. However, Fast uses [Dataflow](https://dataflow.femastudios.com), a commercial library, among its dependencies. In order to compile Fast you need access to that library. You can do so by [contacting FEMA Studios](https://dataflow.femastudios.com/contact_sales).

## Advanced features

### Sorting and pressing enter
Fast remembers which apps you open, so the list of apps is sorted in a useful way. For example, suppose you have installed two apps: "Maps" and "Pac Man", and you open very frequently Maps, and rarely Pac Man. Then, if you type "Ma" on Fast, both the apps are shown, but Maps is first. Then you can press enter on the keyboard, and Maps opens, even if there's no unique matching.

### Hide and rename apps
Unfortunately, it's very common to have installed apps that you rarely or never open. By long pressing an app, you can hide it or rename it. This is particularly useful since Fast works well when there are few matches for any given search query.

For example, suppose you've installed "Boost", a Reddit client, and "Bingo". When you press "B" on Fast, both apps are shown. If you never use Bingo, you can completely hide it, so by typing "b", Boost opens immediately. Alternatively, you can choose to rename "Boost" to "Reddit", so when you type an "r", Boost opens immediately.

### Search on the Web/Youtube/Contacts
You can optionally enable Fast to search on the web, on YouTube™. This works by prefixing your search with a customizable character. By default:
 - *"g <query>"* searches on the web  
 - *"y <query>"* searches on YouTube™  
 - *"c <query>"* searches on your contact list  
